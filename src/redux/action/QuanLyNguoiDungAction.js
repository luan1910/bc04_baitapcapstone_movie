import { quanLyNguoiDungService } from "../../services/QuanLyNguoiDung";
import { DANG_NHAP_ACTION } from "./types/QuanLyNguoiDungType";
import { history } from "../../App";
import { TOKEN, TOKEN_CYBERSOFT, USER_LOGIN } from "../../util/settings/config";
export const dangNhapAction = (thongTinDangNhap) => {
  return async (dispatch) => {
    try {
      const result = await quanLyNguoiDungService.dangNhap(thongTinDangNhap);
      if (result.data.status === 200) {
        dispatch({
          type: DANG_NHAP_ACTION,
          thongTinDangNhap: result.data.content,
        });
      }

      console.log("result: ", result);
      localStorage.setItem(USER_LOGIN, JSON.stringify(result.data.content));
      localStorage.setItem(TOKEN, result.data.content.accessToken);
      history.goBack();
    } catch (error) {
      console.log("error: ", error);
    }
  };
};
