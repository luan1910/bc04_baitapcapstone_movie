import {
  SET_DANH_SACH_PHIM,
  SET_FILM_DANG_CHIEU,
  SET_FILM_SAP_CHIEU,
  SET_THONG_TIN_PHIM,
} from "../action/types/QuanLyPhimType";
import { SET_CHI_TIET_PHIM } from "../action/types/QuanLyRapType";

const stateDefault = {
  arrFilm: [
    {
      maPhim: 4431,
      tenPhim: "Vượt",
      biDanh: "vuot",
      trailer: "https://www.youtube.com/embed/1HpZevFifuo",
      hinhAnh:
        "https://movienew.cybersoft.edu.vn/hinhanh/vuot-qua-song-gio-3-ahhh_gp00.jpg",
      moTa: "HKT vượt qua sóng gió là bộ phim nói về vieecjj nhóm nhạc abcxyz",
      maNhom: "GP00",
      ngayKhoiChieu: "2022-09-30T09:50:45.38",
      danhGia: 1,
      hot: true,
      dangChieu: true,
      sapChieu: true,
    },
    {
      maPhim: 4431,
      tenPhim: "Vượt",
      biDanh: "vuot",
      trailer: "https://www.youtube.com/embed/1HpZevFifuo",
      hinhAnh:
        "https://movienew.cybersoft.edu.vn/hinhanh/vuot-qua-song-gio-3-ahhh_gp00.jpg",
      moTa: "HKT vượt qua sóng gió là bộ phim nói về vieecjj nhóm nhạc abcxyz",
      maNhom: "GP00",
      ngayKhoiChieu: "2022-09-30T09:50:45.38",
      danhGia: 1,
      hot: true,
      dangChieu: true,
      sapChieu: true,
    },
  ],
  dangChieu: true,
  sapChieu: true,
  arrFilmDefault: [],
  filmDetail: {},
  thongTinPhim: {},
};

export const QuanLyPhimReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case SET_DANH_SACH_PHIM: {
      state.arrFilm = action.arrFilm;
      state.arrFilmDefault = state.arrFilm;
      return { ...state };
    }
    case SET_FILM_DANG_CHIEU: {
      state.dangChieu = !state.dangChieu;
      state.arrFilm = state.arrFilmDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };
    }
    case SET_FILM_SAP_CHIEU: {
      state.sapChieu = !state.sapChieu;
      state.arrFilm = state.arrFilmDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };
    }
    case SET_CHI_TIET_PHIM: {
      state.filmDetail = action.filmDetail;
      return { ...state };
    }
    case SET_THONG_TIN_PHIM: {
      state.thongTinPhim = action.thongTinPhim;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
